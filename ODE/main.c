#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

int ode_driver(
        void f(int n, double x, gsl_vector*y, gsl_vector*dydx),
        int n, gsl_vector* xlist, gsl_matrix* ylist,
        double b, double h, double acc, double eps, int max);

int ode_driver2(
        void f(int n, double x, gsl_vector*y, gsl_vector*dydx),
        int n, gsl_vector* xlist, gsl_matrix* ylist,
        double b, double h, double acc, double eps, int max);



int main(){
	int calls = 0;
	void f(int n, double x, gsl_vector* y, gsl_vector* dydx){
		calls++;
		gsl_vector_set(dydx,0, gsl_vector_get(y,1));
		gsl_vector_set(dydx,1, -1*gsl_vector_get(y,0));  // sin function
	}	
	int n=2;
	int max=1000;
	// Allocate xlist and ylist: xlist containing the times in each loop in the driver,
	// ylist containg the solution y and derivative dydx for each loop in the driver.
	// this is already done here to include exercise B in A.
	gsl_vector* xlist = gsl_vector_alloc(max);
	gsl_matrix* ylist = gsl_matrix_alloc(max,n);

	double pi=atan(1.)*4;
	double a=0, b=8*pi, acc=0.01, eps=0.01;
	double h=0.1;
	gsl_vector_set(xlist,0,a); 
	gsl_matrix_set(ylist,0,0,0); 
	gsl_matrix_set(ylist,0,1,1);
	int k = ode_driver(f,n,xlist,ylist,b,h,acc,eps,max);


	if(k<0)printf("max steps reached in ode_driver\n");

	printf("Exercise A: The solution for the differential equation y'' = -y (sine function) found by ODE (rkstep12) with x = %g is y=%g, dydx=%g and number of function calls %i\n",gsl_vector_get(xlist,k-1),gsl_matrix_get(ylist,k-1,0),gsl_matrix_get(ylist,k-1,1),calls);
	printf("The theoretical solution is sin(%g) = %g,\n",gsl_vector_get(xlist,k-1),sin(gsl_vector_get(xlist,k-1)));
	printf("\n");
	printf("Exercise B: The solutions for each loop are stored in the following vectors:\n");
	printf("And the data is plotted, which is seen to give a sine curve as expected.\n");
	printf("\n\n");
	printf("# x, y \n");
	for(int i=0;i<k;i++)printf("%g %g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0));
	printf("\n\n");

	// C
	calls = 0;
	gsl_vector_set_zero(xlist);
	gsl_matrix_set_zero(ylist);
	gsl_vector_set(xlist,0,a); 
	gsl_matrix_set(ylist,0,0,0); 
	gsl_matrix_set(ylist,0,1,1);
	int k2 = ode_driver2(f,n,xlist,ylist,b,h,acc,eps,max);

	printf("Exercise C - higher order stepper \n");
	if(k2<0)printf("max steps reached in ode_driver\n");

	printf("The solution found by ODE stepper rkstep23 with x = %g is y=%g, dydx=%g and number of function calls %i \n", \
		gsl_vector_get(xlist,k2-1),gsl_matrix_get(ylist,k2-1,0), \
		gsl_matrix_get(ylist,k2-1,1), calls);
	printf("The theoretical solution is sin(%g) = %g,\n", \
		gsl_vector_get(xlist,k2-1),sin(gsl_vector_get(xlist,k2-1)));
	printf("The rk23 stepper gives a more precise result and less function calls.\n");
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);
	return 0;
}
