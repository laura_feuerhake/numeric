#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

void rkstep12(void f(int n, double x, gsl_vector*yx, gsl_vector*dydx),
int n, double x, gsl_vector* yx, double h, gsl_vector* yh, gsl_vector* dy){
  gsl_vector* k0 = gsl_vector_alloc(n);
  gsl_vector* yt = gsl_vector_alloc(n);
  gsl_vector* k12 = gsl_vector_alloc(n);
  f(n,x,yx,k0);  
  for(int i=0;i<n;i++){
  	gsl_vector_set(yt,i,gsl_vector_get(yx,i)+ gsl_vector_get(k0,i)*h/2);
  }
  f(n,x+h/2,yt,k12); 
  for(int i=0;i<n;i++){
  	gsl_vector_set(yh,i,gsl_vector_get(yx,i)+gsl_vector_get(k12,i)*h);
  }
  for(int i=0;i<n;i++){
  	gsl_vector_set(dy,i,(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2);
  }
  gsl_vector_free(k0);
  gsl_vector_free(yt);
  gsl_vector_free(k12);
  
}

void rkstep23(void f(int n, double x, gsl_vector*yx, gsl_vector*dydx),
int n, double x, gsl_vector* yx, double h, gsl_vector* yh, gsl_vector* dy){
  gsl_vector* k1 = gsl_vector_alloc(n);
  gsl_vector* k2 = gsl_vector_alloc(n);
  gsl_vector* k3 = gsl_vector_alloc(n);
  gsl_vector* k4 = gsl_vector_alloc(n); 
  gsl_vector* yt = gsl_vector_alloc(n);

  f(n,x,yx,k1);  
  for(int i=0;i<n;i++){
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+ gsl_vector_get(k1,i)*h/2);
  }
  f(n,x+h/2,yt,k2); 
  for(int i=0;i<n;i++){
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+gsl_vector_get(k2,i)*3*h/4);
  }
  f(n,x+3*h/4,yt,k3); 
  for(int i=0;i<n;i++){
    gsl_vector_set(yh,i,gsl_vector_get(yx,i)+gsl_vector_get(k1,i)*2*h/9
      +gsl_vector_get(k2,i)*1*h/3+gsl_vector_get(k3,i)*4*h/9);
  }
  f(n,x+h,yt,k4); 
  for(int i=0;i<n;i++){
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+gsl_vector_get(k1,i)*7*h/24
      +gsl_vector_get(k2,i)*1*h/4+gsl_vector_get(k3,i)*1*h/3+
      gsl_vector_get(k4,i)*1*h/8);
    gsl_vector_set(dy,i,gsl_vector_get(yh,i)-gsl_vector_get(yt,i));
//    gsl_vector_set(dy,i,(gsl_vector_get(yh,i)-gsl_vector_get(yh,i)*1/2-gsl_vector_get(yt,i)*1/2)/(pow(2,2)-1));
  }

  gsl_vector_free(k1);
  gsl_vector_free(k2);
  gsl_vector_free(k3);
  gsl_vector_free(k4);  
  gsl_vector_free(yt);

  
}