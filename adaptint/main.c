#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include <gsl/gsl_integration.h>

double adaptint(double f(double), double a, double b, double acc, double eps, double *err, double QTrue);

int ode_driver(
        void f(int n, double x, gsl_vector*y, gsl_vector*dydx),
        int n, gsl_vector* xlist, gsl_matrix* ylist,
        double b, double h, double acc, double eps, int max);

int main(int argc, char const *argv[]){
	int calls = 0;
	double a = 0,b = 1, err = 0, acc = 0.0001, eps = 0.0001;
	double Q;

	double sqrt_x(double x){
		calls++;
		//QTrue = 2/3;
		return sqrt(x);
	}
double
	 inv_sqrt_x(double x){
		calls++;
		//QTrue = 2;
		return 1/sqrt(x);
	}

	double ln_sqrt_x(double x){
		calls++;
		//QTrue = -4;
		return log(x)/sqrt(x);
	}

	double f_A2(double x){
		calls++;
		//QTrue = -pi;
		return 4*sqrt(1-(1-x)*(1-x));
	}
	double f_B(double x){
		calls++;
		//QTrue = sqrt(pi) = 1.7725;
		//double y = x/(1-pow(x,2));
		return exp(-pow(x,2));
	}

	double f_B2(double x){
		calls++;
		//QTrue = sqrt(pi)/2 = 0.886;
		//double y = x/(1-pow(x,2));
		return pow(x,2)*exp(-pow(x,2));
	}
// A
	// 1. : 
	// Implement a recursive adaptive integrator that estimates the 
	// integral of a given function f(x) on a given interval [a,b] 
	// with the required absolute, acc, or relative, eps, accuracy 
	// goals. 
	printf("Exercise A, part 1: \n The code is tested on following test functions:\n\n");

	double Qt = 2.0/3.0;
	calls = 0;
	Q = adaptint(sqrt_x,a,b,acc,eps,&err, Qt);
	printf("Int_theo(sqrt(x)) = %g, Q = %lg, error = %lg, calls = %i\n\n", Qt,Q,err,calls);
	double QA = Q;
	int callsA = calls;
	double QtA = Qt;

	Q = adaptint(inv_sqrt_x,a,b,acc,eps,&err, 2);
	printf("Int_theo(1/sqrt(x)) = %g, Q = %lg, error = %lg, calls = %i\n\n", 2.0,Q,err,calls);

	Q = adaptint(ln_sqrt_x,a,b,acc,eps,&err, -4);
	printf("Int_theo(ln(x)/sqrt(x)) = %g, Q = %lg, error = %lg, calls = %i\n\n", -4.0,Q,err,calls);

	printf("Exercise A, part 2: \n Following function is integrated:\n\n");

	Qt = M_PI;
	calls = 0;

	Q = adaptint(f_A2,a,b,acc,eps,&err, Qt);
	printf("4*sqrt(1-(1-x)*(1-x)) = %g, Q = %.16f, error = %.16f, and the number of calls = %i\n\n", Qt,Q,err,calls);

//////Exercise B:
	//Generalize your integrator to accept infinite limits.

	printf("Exercise B - test infinite limits \n\n");
	a = -INFINITY;
	b = INFINITY;
	//inf_inf = 1;
	
	Qt = sqrt(M_PI);
	calls = 0;
	Q = adaptint(f_B,a,b,acc,eps,&err, Qt);
	printf("int exp(-x²) from -inf to inf = %g, Q = %lg, error = %lg, and the number of calls = %i\n\n", Qt,Q,err,calls);
	
	Qt = sqrt(M_PI)/2;
	calls = 0;
	Q = adaptint(f_B2,a,b,acc,eps,&err, Qt);
	printf("int x²exp(-x²) from -inf to inf = %g, Q = %lg, error = %lg, and the number of calls = %i\n\n", Qt,Q,err,calls);
	double Qxsq = Q;
	double errxsq = err;
	int callsxsq = calls;

	a = -INFINITY;
	b = 0;
	
	Qt = sqrt(M_PI)/2;
	calls = 0;
	Q = adaptint(f_B,a,b,acc,eps,&err, Qt);
	printf("int exp(-x²) from -inf to 0 = %g, Q = %lg, error = %lg, and the number of calls = %i\n\n", Qt,Q,err,calls);

	a = 0;
	b = INFINITY;
	
	Qt = sqrt(M_PI)/2;
	calls = 0;
	Q = adaptint(f_B,a,b,acc,eps,&err, Qt);
	printf("int exp(-x²) from 0 inf = %g, Q = %lg, error = %lg, and the number of calls = %i\n\n",Qt,Q,err,calls);

	//check with gsl	f_B
	int GSLcalls=0;
	double f (double x, void * params) {
		GSLcalls++;
  		double f = exp(-pow(x,2));
  		return f;
	}

	gsl_integration_workspace * w 
    	= gsl_integration_workspace_alloc (1000);
  
  	double result, error;

  	gsl_function F;
  	F.function = &f;
  	F.params = NULL;

  	gsl_integration_qagil (&F, 0 , eps, acc, 1000,
                        w, &result, &error); 
	Qt = sqrt(M_PI)/2;
  	 printf ("check exp(-pow(x,2)) with gsl_integration_qagil \n \
  		(same acc, eps), result = %.14f, \n \
  		error=%.14f. No of calls = %i .\n \
  		Comparison: my routine gives: = %.14f error = %.14f , \n \
  		and the number of calls = %i\n\n", result, \
  		result-Qt,GSLcalls, Q, err, calls);

  	gsl_integration_workspace_free (w);

  	
	//check with gsl f_B2
	GSLcalls=0;
	double f2 (double x, void * params) {
		GSLcalls++;
  		double f2 = pow(x,2)*exp(-pow(x,2));
  		return f2;
	}

	gsl_integration_workspace * w2 
    	= gsl_integration_workspace_alloc (1000);
  
  	gsl_function F2;
  	F2.function = &f2;
  	F2.params = NULL;

  	gsl_integration_qagi (&F2, eps, acc, 1000,
                        w2, &result, &error); 

	Qt = sqrt(M_PI)/2;
  	 printf ("check pow(x,2)*exp(-pow(x,2)) with gsl_integration_qagil \n \
  		(same acc, eps), result = %.14f, \n \
  		error=%.14f. No of calls = %i .\n \
  		Comparison: my routine gives: = %.14f error = %.14f , \n \
  		and the number of calls = %i\n\n", result, \
  		result-Qt,GSLcalls, Qxsq, errxsq, callsxsq);

  	gsl_integration_workspace_free (w2); 


  	// Exercise C
  	printf("\n\n Exercise C\n");
  	printf("The solution to an integral can also be found by using the ODE routin and solving the diff.eq. dydx = f(x), where f(x)=sqrt(x) in the following\n");
  	calls = 0;
  	void sqrtx_diff(int n,double x,gsl_vector* y, gsl_vector* dydx){
		calls++;
		gsl_vector_set(dydx,0,sqrt(x));
	}

	int n=1, max=1000;

	gsl_vector* xlist = gsl_vector_alloc(max);
	gsl_matrix* ylist = gsl_matrix_alloc(max,n);

	gsl_matrix_set(ylist,0,0,0); 
	gsl_vector_set(xlist,0,0); 

	calls = 0;

	double h=0.1;
	b=1; 

	int k = ode_driver(sqrtx_diff,n,xlist,ylist,b,h,acc,eps,max);

	printf(" The result of  int sqrt(x) with the ODE routine is: Q=%g. Number of calls = %i. My integral routine gives Q = %g with number of calls = %i, the theoretical value is = %g. \n", gsl_matrix_get(ylist,k-1,0),calls,QA, callsA, QtA);


	return 0;
}




