#include<stdlib.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include<math.h>
#include "gsl_laura.h"

void matrix_print(gsl_matrix* A){
	for(int i=0;i < A->size1; i++){
		for(int j=0; j< A->size2; j++){
			printf("%g \t ", gsl_matrix_get(A,i,j));
		}
		printf("\n");
	}
	printf("\n");
}

void vector_print(gsl_vector* b){
	printf("(");
	for(int i=0;i < b->size; i++){
			printf(" %g ", gsl_vector_get(b,i));
		}
	printf(")\n\n");
}

double vector_norm(gsl_vector* x){
	int n=x->size;
	double sum=0;
	for(int i=0;i<n;i++){
		sum+=pow(gsl_vector_get(x,i),2);
	}
	return pow(sum,0.5);
}

double vector_dot_product(gsl_vector* x, gsl_vector* y){
	int n=x->size;
	double sum=0;
	for(int i=0;i<n;i++){
		sum += gsl_vector_get(x,i) * gsl_vector_get(y,i);
	}
	return sum;
}

void vector_outer_product(gsl_matrix* OUT, gsl_vector* l, gsl_vector* r){
	int n=l->size;
	for(int i=0;i<n;i++){
		for (int j = 0; j < n; ++j){
			gsl_matrix_set(OUT,i,j, gsl_vector_get(l,i) * gsl_vector_get(r,j));
		}		
	}
}

void matrix_sum(gsl_matrix* OUT, gsl_matrix* A, gsl_matrix* B){
	int m=OUT->size2, n=OUT->size1;
	for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){
			gsl_matrix_set(OUT,i,j, gsl_matrix_get(A,i,j) + gsl_matrix_get(B,i,j));
		}
	}	
}

void matrix_vector_prod(gsl_vector* out, gsl_matrix* A, gsl_vector* b){
	gsl_vector* c = gsl_vector_alloc(A->size1);
	for (int i = 0; i < A->size1; i++){
		double P=0;
		for(int j=0;j < b->size; j++){
			P+=gsl_matrix_get(A,i,j)*gsl_vector_get(b,j);
		}
		gsl_vector_set(c,i,P);
	}
	for (int i = 0; i < c->size; ++i){
		gsl_vector_set(out,i,gsl_vector_get(c,i));
	}
}

void matrix_prod(gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){
	int m=OUT->size2, n=OUT->size1;
	for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){
			double sum=0;
			for(int k=0;k<L->size2;k++){
			sum+=gsl_matrix_get(L,i,k)*gsl_matrix_get(R,k,j); //sum+=L(n,i)*R(i,m)
			}
			gsl_matrix_set(OUT,i,j,sum);
		}
	}
}
