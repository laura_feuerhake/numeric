void matrix_print(gsl_matrix* A);
void vector_print(gsl_vector* b);
double vector_norm(gsl_vector* x);
double vector_dot_product(gsl_vector* x, gsl_vector* y);
void matrix_vector_prod(gsl_vector* out,gsl_matrix* A, gsl_vector* b);
void vector_outer_product(gsl_matrix* OUT, gsl_vector* l, gsl_vector* r);
void matrix_prod(gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R);
void matrix_sum(gsl_matrix* OUT, gsl_matrix* A, gsl_matrix* B);

