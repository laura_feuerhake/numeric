#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include <gsl/gsl_integration.h>
#include "gsl_laura.h"
#include "quasi_newton.h"
#include "gsl/gsl_multimin.h"

int main(){
	FILE* datafile;
    datafile = fopen("datafile.txt","w");

	printf("Exercise 18: Quasi Newton minimization with BFGS update\n\n");
	printf("In this exercise the quasi newton method is implemented for some minimization problems.\n");
	printf("The theory of the Quasi Newton method can be found in the chapter 'optimization'.\n");
	printf("The updates are done by the BFGS update which means that the update of the inverse of the Hessian is:\n");
	printf("H1 -> H1 + dH = H1 + (sTy + yTH1y)*ssT / (sTy)^2 - (H1ysT + syTH1)/sTy, \n");
	printf("where H1 denotes the inverse of the Hessian matrix H, \n");
	printf("vectors are s = -H1*gradf, y with  \n");
	printf("y(k) = gradf(x_(k+1)) - gradf(x_k) and x_(k+1) = x_k + s_k. \n");
	printf("sT and yT denote the transpose vectors of s and y. \n");
	printf("\n");
	printf("To check my implementation I will find minima of some interesting functions\n\n");
	printf("In part A the method is tested on the Rosenbrock function, the Himmelblau function and the function f(x,y,z)=x²+y²+z².\n");
	printf("In part B the results are compared to results of the quasi-newton method with Broyden update from the optimization exercise");
	printf(" and the gsl routine gsl_multimin_fdfminimizer_vector_bfgs2 which uses the quasi newton method with BFGS update.\n");
	printf("In part C, the quasi newton method is used to minimize the least squares function, and a least squares fit is done ");
	printf("for data from radioactive decay, with the fit function A*exp(-t/T)+B. \n");
	printf("\n Part A)\n");
	printf("The quasiNewton method with BFGS gives following results with epsilon=10^⁻4 and stepsize dx = 10^⁻7.:\n\n");

    
	gsl_vector* xstart = gsl_vector_alloc(2);
	gsl_vector* xstart4 = gsl_vector_alloc(3);
	double eps = 0.0001;
	int steps;
	double dx = 0.0000001;

	gsl_vector_set(xstart,0,0);
	gsl_vector_set(xstart,1,0);
	steps = quasiNewton(rosenbrock, xstart, dx, eps);
	printf("The local minimum of the Rosenbrock function with startvalues (0,0) and number of steps %i is:\n",steps);
	printf("x=%g\n", gsl_vector_get(xstart,0));
	printf("y=%g\n", gsl_vector_get(xstart,1));

	gsl_vector_set(xstart,0,-5);
	gsl_vector_set(xstart,1,-3);
	steps = quasiNewton(himmelblau, xstart, dx, eps);
	printf("\nThe local minimum of the Himmelblau function with startvalues (-5,-3) and number of steps %i is:\n",steps);
	printf("x=%.8f\n", gsl_vector_get(xstart,0));
	printf("y=%.8f\n", gsl_vector_get(xstart,1));

	gsl_vector_set(xstart4,0,-200);
	gsl_vector_set(xstart4,1,3);
	gsl_vector_set(xstart4,2,10);	
	steps = quasiNewton(fsphere, xstart4, dx, eps);
	printf("\nThe minimum of the function f(x,y,z)=x²+y²+z² with startvalues (-200,3,10) and number of steps %i is:\n",steps);
	printf("x=%.8f\n", gsl_vector_get(xstart4,0));
	printf("y=%.8f\n", gsl_vector_get(xstart4,1));
	printf("z=%.8f\n", gsl_vector_get(xstart4,2));

/// B ///
	printf("\nPart B)\n");
	printf("\nAs a comparison, the minima found by the quasi Newton method with Broydens update in the optimization exercise are as follows: \n");
	
	printf("\nThe local minimum in the Rosenbrock function with startvalues (0,0) is  x=0.999912,y=0.999823 and the number of steps is: 6176\n");
	printf("Theoretically this minimum is exactly at (1,1).\n");
	printf("This leads to the conclusion that Broydens update gives a less precise result and more steps for the rosenbrock function.\n");

	printf("\nThe local minimum in the Himmelblau function with startvalues (-5,-3) is: x=-3.77930985,y=-3.28318608 and the number of steps is: 8.\n");
	printf("Wikipedia and wolfram alpha: The local minimum in the Himmelblau function is at x=-3.77931025,y=-3.283186 which is assumed to be the true value.\n");
	printf("The quasi newton method with BFGS update seems to be more precise.\n");
	printf("It has a more precise value of the x value and needs the same amount of steps.\n");

	printf("\nThe gsl routine gsl_multimin is now implemented with the bfgs2 update and with with recommended stepsize 0.01.\n");

 // rosenbrock:
	int iter = 0;
	int status;
	double stepsize = 0.01;

	gsl_multimin_fdfminimizer* s = gsl_multimin_fdfminimizer_alloc(gsl_multimin_fdfminimizer_vector_bfgs2,2);
	gsl_multimin_function_fdf F;
	F.n = 2;
	F.f = gsl_rosenbrock;
	F.df = dfrosenbrock;
	F.fdf = rosenbrock_fdf;
	F.params = NULL;

 	gsl_vector_set(xstart,0,0);
	gsl_vector_set(xstart,1,0);

	gsl_multimin_fdfminimizer_set(s,&F,xstart,stepsize,0.001);
	do{
      	iter++;
      	status = gsl_multimin_fdfminimizer_iterate (s);
      	if (status)
        	break;
      	status = gsl_multimin_test_gradient(s->gradient, 1e-3);
      	if (status == GSL_SUCCESS)
        	printf ("\nThe local gsl-minimum of the rosenbrock function is found at:\n");
    }while (status == GSL_CONTINUE && iter < 100);

	printf ("x=%.8f y=%.8f with number of steps : %i. \n",
  		gsl_vector_get (s->x, 0), 
  		gsl_vector_get (s->x, 1), iter);

	gsl_multimin_fdfminimizer_free(s);

// himmelblau:

	iter = 0;
	status = 0;
	gsl_multimin_fdfminimizer* sh = gsl_multimin_fdfminimizer_alloc(gsl_multimin_fdfminimizer_vector_bfgs2,2);
	gsl_multimin_function_fdf G;
	G.n = 2;
	G.f = gsl_himmelblau;
	G.df = dfhimmelblau;
	G.fdf = himmelblau_fdf;
	G.params = NULL;
	gsl_vector_set(xstart,0,-5);
	gsl_vector_set(xstart,1,-3);
	gsl_multimin_fdfminimizer_set(sh,&G,xstart,stepsize,0.001);
	do{
      	iter++;
      	status = gsl_multimin_fdfminimizer_iterate (sh);
      	if (status)
        	break;
      	status = gsl_multimin_test_gradient(sh->gradient, 1e-3);
      	if (status == GSL_SUCCESS)
	       	printf ("\nThe local gsl-minimum of the himmelblau function is found at:\n");
    }while (status == GSL_CONTINUE && iter < 100);

	printf ("x=%.8f y=%.8f with number of steps : %i. \n",
  		gsl_vector_get (sh->x, 0), 
  		gsl_vector_get (sh->x, 1), iter);

	gsl_multimin_fdfminimizer_free(sh);

// sphere

	iter = 0;
	status = 0;
	gsl_multimin_fdfminimizer* sp = gsl_multimin_fdfminimizer_alloc(gsl_multimin_fdfminimizer_vector_bfgs2,3);
	gsl_multimin_function_fdf S;
	S.n = 3;
	S.f = gsl_fsphere;
	S.df = dfsphere;
	S.fdf = sphere_fdf;
	S.params = NULL;
	gsl_vector_set(xstart4,0,-200);
	gsl_vector_set(xstart4,1,3);
	gsl_vector_set(xstart4,2,10);
	gsl_multimin_fdfminimizer_set(sp,&S,xstart4,stepsize,0.001);
	do{
      	iter++;
      	status = gsl_multimin_fdfminimizer_iterate (sp);
      	if (status)
        	break;
      	status = gsl_multimin_test_gradient(sp->gradient, 1e-3);
      	if (status == GSL_SUCCESS)
	       	printf ("\nThe local gsl-minimum of the function f(x,y,x)=x²+y²+z² is found at:\n");
    }while (status == GSL_CONTINUE && iter < 100);

	printf ("x=%.14f y=%.14f with number of steps : %i. \n",
  		gsl_vector_get (sp->x, 0), 
  		gsl_vector_get (sp->x, 1), iter);

	gsl_multimin_fdfminimizer_free(sp);



	printf("\nYou see that the gsl routine gives the most accurate results with least number iterations for the rosenbrock function and the function f(x,y,z) = x²+y²+z², while the minimum in the himmelblau function is not as exact but uses less steps.\n");




//////// leastsquaresfit: //////

	

	printf("\n\n");
	printf("Part C) \n\n");
	printf("Now the Quasi Newton metod is used to find the best least squares fit to experimental data with the fit function A*exp(i/T)+B. Startvalue is A=T=B=1.\n");
	printf("This example is taken from the minimization exercise.\n");
	gsl_vector* xstart_exp = gsl_vector_alloc(3);
	gsl_vector_set(xstart_exp,0,1);
	gsl_vector_set(xstart_exp,1,1);
	gsl_vector_set(xstart_exp,2,1);
	steps = quasiNewton(f_exp, xstart_exp, dx,eps);
	printf("The fitting parameters are as follows:\n");
	printf("A=%g \t T=%g \t B=%g \t steps=%i\n",gsl_vector_get(xstart_exp,0),gsl_vector_get(xstart_exp,1),gsl_vector_get(xstart_exp,2),steps);
	printf("The plot shows that the fit is within the uncertainties.\n");

	fprintf(datafile,"#x values and fitted function values in the interval t=0 to t=10:\n");
	double A = gsl_vector_get(xstart_exp,0);
	double T = gsl_vector_get(xstart_exp,1);
	double B = gsl_vector_get(xstart_exp,2);
	fprintf(datafile," #t \t f(t)\n");
	for(double i=0;i<10;i+=0.05){
		fprintf(datafile,"%g \t %g\n",i,A*exp(-i/T)+B);
	}
	fprintf(datafile, "\n\n");
	fprintf(datafile,"#experimental values\n");
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
	fprintf(datafile,"#t \t y \t e\n");
	for(int i=0;i<N;i++){	
		double tp = t[i];
		double yp = y[i];
		double ep = e[i];
		fprintf(datafile,"%g \t %g \t %g\n",tp,yp,ep);
	}

	fclose(datafile);

		// See the plot



	gsl_vector_free(xstart);
	gsl_vector_free(xstart_exp);	

	return 0;
}

