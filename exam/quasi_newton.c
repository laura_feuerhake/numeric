#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include <gsl/gsl_integration.h>
#include "gsl_laura.h"
#include "quasi_newton.h"

// FUNCTION DEFINITIONS:

double rosenbrock(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((1-x0),2) + 100*pow((x1-x0*x0),2);
	return f;
}

double himmelblau(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((x1+x0*x0-11),2) + pow((x0+x1*x1-7),2);
	return f;
}

double fsphere(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double x2 = gsl_vector_get(x,2);
	double f = pow(x0,2) + pow(x1,2) + pow(x2,2);
	return f;
}

double gsl_rosenbrock(const gsl_vector* x, void* params){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((1-x0),2) + 100*pow((x1-x0*x0),2);
	return f;
}

void rosenbrock_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g){
	*f = gsl_rosenbrock(x,params);
	dfrosenbrock(x,params,g);
}

void dfrosenbrock(const gsl_vector* x, void* params, gsl_vector* g){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 2*(x0-1) - 400*(x1-pow(x0,2))*x0;
	double f1 = 200*(x1 - pow(x0,2));
	gsl_vector_set(g,0,f0);
	gsl_vector_set(g,1,f1);	
}

double gsl_himmelblau(const gsl_vector* x, void* params){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((x1+x0*x0-11),2) + pow((x0+x1*x1-7),2);
	return f;
}

void himmelblau_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g){
	*f = gsl_himmelblau(x,params);
	dfhimmelblau(x,params,g);
}

void dfhimmelblau(const gsl_vector* x, void* params, gsl_vector* g){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(pow(x0,2)+x1-11) + 2*(x0+pow(x1,2)-7);
	double f1 = 2*(pow(x0,2)+x1-11) + 4*x1*(x0+pow(x1,2)-7);
	gsl_vector_set(g,0,f0);
	gsl_vector_set(g,1,f1);
}

double gsl_fsphere(const gsl_vector* x, void* params){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double x2 = gsl_vector_get(x,2);
	double f = pow(x0,2) + pow(x1,2) + pow(x2,2);
	return f;
}

void sphere_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g){
	*f = gsl_fsphere(x,params);
	dfsphere(x,params,g);
}

void dfsphere(const gsl_vector* x, void* params, gsl_vector* g){
	gsl_vector_memcpy(g,x);
	gsl_vector_scale(g,2);
}

double f_exp(gsl_vector* x){
	// least squares function
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	double sum = 0;
	for(int i=0;i<N;i++){
		double f = A*exp(-t[i]/T)+B;
		sum +=  pow(f-y[i],2)/pow(e[i],2);
	}
	return sum;
}

void gradient(double f(gsl_vector* x), gsl_vector* x, double dx,gsl_vector* grad_f){
	double fx=f(x);
	for (int i=0; i<x->size; i++){
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
		gsl_vector_set(grad_f,i,(f(x)-fx)/dx);
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
	}
}

// THE MINIMIZATION ALGORITHM

int quasiNewton(double f(gsl_vector* x), gsl_vector* x, double dx, double eps){
	/* This function finds the minimum of a function by using the gradient and
	making updates on its Hessian matrix in the quasi newton method.
	These updates are made with the BFGS method using the expansion from the wikipedia article:
	H1 -> H1 + dH = H1 + (sTy + yTH1y)*ssT / (sTy)^2 - (H1ysT + syTH1)/sTy
	where H1 denotes the inverse of the Hessian matrix H,
	vectors are s = -H1*gradf, y with 
	y(k) = gradf(x_(k+1)) - gradf(x_k) and x_(k+1) = x_k + s_k.
	sT and yT denote the transpose vectors of s and y.
	*/

	// Initialization:
	int n=x->size;	
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* grad_f = gsl_vector_alloc(n);
	gsl_vector* grad_fz = gsl_vector_alloc(n);
	gsl_matrix* H1 = gsl_matrix_alloc(n,n);
	gsl_matrix* SS = gsl_matrix_alloc(n,n);
	gsl_matrix* ysT = gsl_matrix_alloc(n,n);
	gsl_matrix* H1ysT = gsl_matrix_alloc(n,n);
	gsl_matrix* syTH1 = gsl_matrix_alloc(n,n);	
	gsl_matrix* dH = gsl_matrix_alloc(n,n);
	gsl_vector* v = gsl_vector_alloc(n);
	double fx, fz, vector_dot;

	gradient(f,x,dx,grad_f);
	gsl_matrix_set_identity(H1); // H_0 = identity
	fx = f(x);
	int nsteps=0;
	
	do{
		nsteps++;  // count the loops to converge
		
		// calculate s: 
		matrix_vector_prod(Dx,H1,grad_f); 
		gsl_vector_scale(Dx,-1);
		gsl_vector_memcpy(s,Dx);
		gsl_vector_scale(s,2);
		
		double alpha=0.001; // set a convergence tolerance for the criterion
							// f(x-s) < f(x) + alpha * sTgradf
		// Find z:
		do{
			gsl_vector_scale(s,0.5);
			for (int i=0; i<n; i++){
				gsl_vector_set(z,i,gsl_vector_get(x,i)+gsl_vector_get(s,i)); // z = x-H1*gradf
			} 
			fz=f(z);
			vector_dot = vector_dot_product(s,grad_f);
			if( fabs(fz) < fabs(fx) + alpha * vector_dot){
				break;
			}
			if(vector_norm(s) < dx) {
				gsl_matrix_set_identity(H1); 
				break;
			}
		}while(fabs(fz) > fabs(fx) + alpha * vector_dot);
	
		gradient(f,z,dx,grad_fz);
		for (int i=0; i<n; i++) {
			gsl_vector_set(y,i,gsl_vector_get(grad_fz,i)-gsl_vector_get(grad_f,i));
		}
		
	// BFGS update: Do the calculation of dH
		matrix_vector_prod(v,H1,y); 			// H1s
		vector_outer_product(SS,s,s);  			// ssT
		vector_outer_product(ysT,y,s);  		// ysT outer product
		double sTy = vector_dot_product(s,y); 	// sTy dot product
		double yTv = vector_dot_product(y,v); 	// yT(H1y)
		matrix_prod(H1ysT,H1,ysT); 				// H1ysT
		gsl_matrix_transpose_memcpy(syTH1,H1ysT); // syTH1 = (H1ysT)T

		gsl_matrix_scale(SS, (sTy + yTv)/pow(sTy,2)); // first part of dH
		matrix_sum(dH,H1ysT,syTH1); 
		gsl_matrix_scale(dH,-1/sTy); 				  // second part of dH
		gsl_matrix_add(dH,SS); 						  // dH = first part + second part

		// update H1, x and gradf(x):
		gsl_matrix_add(H1,dH);
		gsl_vector_memcpy(x,z);  			// x -> z
		gsl_vector_memcpy(grad_f,grad_fz);  // gradf(x) -> gradf(z)
		fx=fz;
	}while(vector_norm(Dx)>dx && vector_norm(grad_f)>eps);
	// Convergence: when stepsize->0 (<dx) and the gradient->0 (<eps)

	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(grad_f);
	gsl_vector_free(grad_fz);
	gsl_vector_free(v);
	gsl_matrix_free(H1);	
	gsl_matrix_free(SS);
	gsl_matrix_free(ysT);	
	gsl_matrix_free(H1ysT);
	gsl_matrix_free(dH);
	
	return nsteps;
}
