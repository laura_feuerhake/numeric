
double rosenbrock(gsl_vector* x);
double himmelblau(gsl_vector* x);
double fsphere(gsl_vector* x);
double f_exp(gsl_vector* x);

void gradient(double f(gsl_vector* x), gsl_vector* x, double dx,gsl_vector* grad_f);

int quasiNewton(double f(gsl_vector* x), gsl_vector* x, double dx, double eps);

double gsl_rosenbrock(const gsl_vector* x, void* params);
void rosenbrock_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g);
double gsl_himmelblau(const gsl_vector* x, void* params);
double gsl_fsphere(const gsl_vector* x, void* params);
void dfrosenbrock(const gsl_vector* x, void* params, gsl_vector* g);
void dfhimmelblau(const gsl_vector* x, void* params, gsl_vector* g);
void dfsphere(const gsl_vector* x, void* params, gsl_vector* g);
void himmelblau_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g);
void sphere_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g);
