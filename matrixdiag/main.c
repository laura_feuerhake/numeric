#include "stdlib.h"
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl_laura.h"
#include "math.h"
#include "time.h"
#define RND (double)rand()/RAND_MAX

int main(){
	int n=4;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* C = gsl_matrix_alloc(n,n);
	gsl_matrix* CT = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* AVD = gsl_matrix_alloc(n,n);
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < n; ++j){
			gsl_matrix_set(C,i,j,RND);
		}
	}
	gsl_matrix_transpose_memcpy(CT,C);
	matrix_prod(A,CT,C);
	printf("Decompose the symmetric matrix A into A=VDV^T \n");
	printf("The random symmetric matrix A is =\n");
	matrix_print(A);

	clock_t clockA_start, clockA_end, clockA_total;
	clockA_start = clock();
    int h = jacobi(A, e, V); //The calculation is stored in the Amatrix upper diagonal, such that the A matrix can be restored
	clockA_end = clock();
	clockA_total = (clockA_end-clockA_start);

	printf("A with upper triangular eliminated by cyclic sweeps =\n");
	matrix_print(A);
	printf("The ortogonal matrix V =\n");
	matrix_print(V);
	printf("The diagonal elements of the diagonal matrix D = V^TAV = \n");
	vector_print(e);


	// restore A:
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
			gsl_matrix_set(A,i,j,gsl_matrix_get(A,j,i));
		}
	}

	printf("Check that D == V^TAV\n");
	gsl_matrix_transpose_memcpy(D,V); // Here D denotes V^T
	matrix_prod(AVD,D,A); // AVD = V^TA
	matrix_prod(D,AVD,V); // D is the diagonal matrix D = V^TAV
	printf("D =\n");
	matrix_print(D);

//B
	printf("\n\n");
	printf("Exercise B:\n A with cyclic sweeps only over individual rows");
	int h_single = 0;
	int h_s;
	gsl_vector* sweeps_vec = gsl_vector_alloc(A->size1);
	
	//
	printf("Alternative B\n");
	int nn=A->size1;
	for (int i = 0; i < n; ++i){ //restore A
		for (int j = i; j < n; ++j){
			gsl_matrix_set(A,i,j,gsl_matrix_get(A,j,i));
		}
	}
	
	clock_t clockB_start, clockB_end, clockB_total;
	clockB_start = clock();
	for (int i = 0; i < nn; ++i){
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
	}
	h_single = 0;
	for (int i = 1; i <= A->size1; ++i){
		h_s = jacobi2(A, e, V, i-1, sweeps_vec);
		h_single += h_s;
		printf("A with the %i. row eliminated  =\n",i);
		matrix_print(A);
		printf("number of sweeps = %i\n", h_s);
		printf("The %i. eigenvalue = %g\n and the eigenvalue vector which is total until the %i. element\n",i,gsl_vector_get(e,i-1),i+1);
		vector_print(e);
	}
	clockB_end = clock();
	clockB_total = (clockB_end-clockB_start);
	//
	int sweeps1 = gsl_vector_get(sweeps_vec,0);
	printf("The angle lies between 0 and pi/2.\n This means, that for p=0, there is always something substracted from the first diagonal element,\n while it is added to the other diagonal element.\n This mean the first diagonal element will store the lowest eigenvalue \n");
	printf("For obtaining the highest eigenvalue first, phi -> phi + pi/2,\n so the signs change in app and aqq and the first diagonal element will have something added.\n");
	printf("The number of sweeps to diagonalize the full matrix from the first part = %lu and the time is %ld\n", h*A->size1,clockA_total);
	printf("The number of sweeps to diagonalize the full matrix value by value = %i and the time is %ld \n", h_single, clockB_total);
	printf("The number of sweeps to calculate the first eigenvalue with value by value method = %i\n", sweeps1);
	
	gsl_matrix_free(A);
	gsl_matrix_free(C);
	gsl_matrix_free(CT);
	gsl_matrix_free(V);		
	gsl_matrix_free(D);
	gsl_vector_free(e);
	gsl_vector_free(sweeps_vec);	
	gsl_matrix_free(AVD);
	return 0;
}