#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdlib.h>
#include "optim.h"

int main(){
	gsl_vector* xstart = gsl_vector_alloc(2);
	double eps = 0.0001;
	int steps;
	steps = newtonMinimization(rosenbrock, dfrosenbrock, Hrosenbrock, xstart, eps);
	printf("Exercise A - find the minimum by Newton minimization\n");
	printf("The minimum in the Rosenbrock function with startvalues (0,0) is:\n");
	printf("x=%g\n", gsl_vector_get(xstart,0));
	printf("y=%g\n", gsl_vector_get(xstart,1));
	printf("The number of steps is: %i\n", steps);
	gsl_vector_set(xstart,0,-5);
	gsl_vector_set(xstart,1,-3);
	steps =newtonMinimization(himmelblau, dfhimmelblau, Hhimmelblau, xstart, eps);
	printf("The minimum in the Himmelblau function with startvalues (-5,-3) is:\n");
	printf("x=%g\n", gsl_vector_get(xstart,0));
	printf("y=%g\n", gsl_vector_get(xstart,1));
	printf("The number of steps is: %i\n\n", steps);

// B
	printf("Exercise B\n\n");
	printf("The quasiNewton method gives following results:\n");
	eps = 0.0001;
	double dx = 0.0000001;
	gsl_vector_set(xstart,0,0);
	gsl_vector_set(xstart,1,0);
	steps = quasiNewton(rosenbrock, xstart, dx, eps);
	printf("The minimum in the Rosenbrock function with startvalues (0,0) is:\n");
	printf("x=%g\n", gsl_vector_get(xstart,0));
	printf("y=%g\n", gsl_vector_get(xstart,1));
	printf("The number of steps is: %i\n You see, that the number of steps with the Quasi Newton method is much bigger than for the newton minimization\n", steps);

	gsl_vector_set(xstart,0,-5);
	gsl_vector_set(xstart,1,-3);
	steps = quasiNewton(himmelblau, xstart, dx, eps);
	printf("The minimum in the Himmelblau function with startvalues (-5,-3) is:\n");
	printf("x=%g\n", gsl_vector_get(xstart,0));
	printf("y=%g\n", gsl_vector_get(xstart,1));
	printf("The number of steps is: %i, which is more than for the newton minimization\n", steps);

	printf("The result of the Roots-exercise was the minimum of the Rosenbrock function with 145 steps. This is higher than the Newton metod, but lower then the Quasi-Newton-metod. In the Roots execise, the minimum of the Himmelblau function was found in 5 steps. Det er lower than for both the Newton og Quasi Newton metod.\n\n");


	printf("Exercise B iv:\n");
	printf("The Quasi-Newton-metoden is used to find the best fit to experimental data with the function A*exp(-i/T)+B. Startvalue is A=T=B=1.\n");
	gsl_vector* xstart_exp = gsl_vector_alloc(3);
	gsl_vector_set(xstart_exp,0,1);
	gsl_vector_set(xstart_exp,1,1);
	gsl_vector_set(xstart_exp,2,1);
	steps = quasiNewton(f_exp, xstart_exp, dx,eps);
	printf("The fitting parameters are as follows:\n");
	printf("A=%g \t T=%g \t B=%g \t steps=%i\n",gsl_vector_get(xstart_exp,0),gsl_vector_get(xstart_exp,1),gsl_vector_get(xstart_exp,2),steps);
	
	printf("\n\n");
	printf("#x values and fitted function values in the interval t=0 to t=10:\n");
	double A = gsl_vector_get(xstart_exp,0);
	double T = gsl_vector_get(xstart_exp,1);
	double B = gsl_vector_get(xstart_exp,2);
	printf(" #t \t f(t)\n");
	for(double i=0;i<10;i+=0.05){
		printf("%g \t %g\n",i,A*exp(-i/T)+B);
	}
	printf("\n\n");
	printf("#experimental values\n");
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	fprintf(stdout,"#t \t y \t e\n"); //print values to stdout to plot
	for(int i=0;i<N;i++){	
		double tp = t[i];
		double yp = y[i];
		double ep = e[i];
		printf("%g \t %g \t %g\n",tp,yp,ep);
	}

		// See the plot

	gsl_vector_free(xstart);
	gsl_vector_free(xstart_exp);	




	return 0;
}

