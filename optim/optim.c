#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdlib.h>
#include "optim.h"

double vector_norm(gsl_vector* x){
	int n=x->size;
	double sum=0;
	for(int i=0;i<n;i++){
		sum+=pow(gsl_vector_get(x,i),2);
	}
	return pow(sum,0.5);
}

void matrix_vector_prod(gsl_matrix* A, gsl_vector* b, gsl_vector* out){
	gsl_vector* c = gsl_vector_alloc(A->size1);
	for (int i = 0; i < A->size1; i++){
		double P=0;
		for(int j=0;j < b->size; j++){
			P+=gsl_matrix_get(A,i,j)*gsl_vector_get(b,j);
		}
		gsl_vector_set(c,i,P);
	}
	for (int i = 0; i < c->size; ++i){
		gsl_vector_set(out,i,gsl_vector_get(c,i));
	}
}

void matrix_prod(gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){
	int m=OUT->size2, n=OUT->size1;
	for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){
			double sum=0;
			for(int k=0;k<L->size2;k++){
			sum+=gsl_matrix_get(L,i,k)*gsl_matrix_get(R,k,j); //sum+=L(n,i)*R(i,m)
			}
			gsl_matrix_set(OUT,i,j,sum);
		}
	}
}


double rosenbrock(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((1-x0),2) + 100*pow((x1-x0*x0),2);
	return f;
}

double himmelblau(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f = pow((x1+x0*x0-11),2) + pow((x0+x1*x1-7),2);
	return f;
}

void dfrosenbrock(gsl_vector* x, gsl_vector* df){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 2*(x0-1) - 400*(x1-pow(x0,2))*x0;
	double f1 = 200*(x1 - pow(x0,2));
	gsl_vector_set(df,0,f0);
	gsl_vector_set(df,1,f1);	
}

void dfhimmelblau(gsl_vector* x, gsl_vector* df){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(pow(x0,2)+x1-11) + 2*(x0+pow(x1,2)-7);
	double f1 = 2*(pow(x0,2)+x1-11) + 4*x1*(x0+pow(x1,2)-7);
	gsl_vector_set(df,0,f0);
	gsl_vector_set(df,1,f1);	
}

void Hrosenbrock(gsl_vector* x, gsl_matrix* H){
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);
	double H00, H11, H01, H10;
	H00 = 2-400*(x1-3*pow(x0,2));
	H01 = -400*x0;
	H10 = -400*x0;
	H11 = 200;
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
}

void Hhimmelblau(gsl_vector* x, gsl_matrix* H){
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);
	double H00, H11, H01, H10;
	H00 = 12*pow(x0,2) + 4*x1 - 42;
	H01 = 4*(x0 + x1);
	H10 = 4*(x0 + x1);
	H11 = 12*pow(x1,2) + 4*x0 - 26;
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
}

double f_exp(gsl_vector* x){
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	double sum = 0;
	for(int i=0;i<N;i++){
		double f=A*exp(-t[i]/T)+B;
		sum +=  pow(f-y[i],2)/pow(e[i],2);
	}
	return sum;
}

void gradient(double f(gsl_vector* x), gsl_vector* x, double dx,gsl_vector* grad_f){
	double fx=f(x);
	for (int i=0; i<x->size; i++){
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
		gsl_vector_set(grad_f,i,(f(x)-fx)/dx);
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
	}
}

void givens_qr(gsl_matrix* A){
	for(int q=0;q<A->size2;q++){
		for(int p=q+1;p<A->size1;p++){
			double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q));
			for(int k=q;k<A->size2;k++){
				double xq=gsl_matrix_get(A,q,k);
				double xp=gsl_matrix_get(A,p,k);
				gsl_matrix_set(A,q,k,xq*cos(theta)+xp*sin(theta));
				gsl_matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta));
			}
			gsl_matrix_set(A,p,q,theta); 
		}
	}
}


void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v){
	for(int q=0; q<QR->size2; q++){
		for(int p=q+1;p<QR->size1;p++){
			double theta=gsl_matrix_get(QR,p,q);
			double vq=gsl_vector_get(v,q);
			double vp=gsl_vector_get(v,p);
			gsl_vector_set(v,q,vq*cos(theta)+vp*sin(theta));
			gsl_vector_set(v,p,-vq*sin(theta)+vp*cos(theta));
		}
	}
}



void givens_qr_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x){
	givens_qr_QTvec(QR,b); 
	for (int i=QR->size2-1; i>=0; i--){ //back-substitution
		double s=0;
		for(int k=i+1; k<QR->size2; k++) s+=gsl_matrix_get(QR,i,k)*gsl_vector_get(x,k);
		gsl_vector_set(x,i,(gsl_vector_get(b,i)-s)/gsl_matrix_get(QR,i,i));
	}
}

int newtonMinimization(double f(gsl_vector* x), void gradient(gsl_vector *x, gsl_vector* df), void hessian(gsl_vector* x,gsl_matrix* H), gsl_vector* xstart, double eps){
	int n=xstart->size;
	gsl_vector* Dx  = gsl_vector_alloc(n);
	gsl_vector* y   = gsl_vector_alloc(n);
	gsl_matrix* H   = gsl_matrix_alloc(n,n);
	gsl_vector* HDx = gsl_vector_alloc(n);
	double fx, Dxd;
	gsl_vector* dfxxx = gsl_vector_alloc(n);
	gsl_vector* dfx   = gsl_vector_alloc(n);
	gsl_vector* df    = gsl_vector_alloc(n);
	double dx = 0.0000001;
	int steps = 0;
	do{
		steps++;
		fx = f(xstart);
		gradient(xstart,dfx);
		hessian(xstart,H);

		givens_qr(H);
		gsl_vector_scale(dfx,-1.0);
		givens_qr_solve(H,dfx,Dx);
		gsl_vector_scale(dfx,-1.0);
		matrix_vector_prod(H,Dx,HDx);
		for (int i = 0; i < n; i++){
			gsl_vector_set(dfxxx,i,gsl_vector_get(dfx,i)+gsl_vector_get(HDx,i));
		}
		double lambda=2;
		do{
			lambda/=2;
			gsl_vector_memcpy(y,Dx);
			gsl_vector_scale(y,lambda);
			gsl_vector_add(y,xstart);
			Dxd = 0;
			for (int i = 0; i < n; ++i){
				Dxd+=gsl_vector_get(Dx,i)*gsl_vector_get(dfx,i);
			}
		}while(fabs(f(y)) > fabs(fx) + 0.0001*lambda*(Dxd)  && lambda>0.02);
		gsl_vector_memcpy(xstart,y);	
	}while(vector_norm(Dx)>dx && vector_norm(dfx)>eps);

	gsl_vector_free(Dx);
	gsl_vector_free(y);
	gsl_vector_free(df);
	gsl_matrix_free(H);
	gsl_vector_free(HDx);
	gsl_vector_free(dfxxx);
	return steps;
}


int quasiNewton(double f(gsl_vector* x), gsl_vector* x, double dx, double eps){
	
	int n=x->size;	
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* grad_f = gsl_vector_alloc(n);
	gsl_vector* grad_fy = gsl_vector_alloc(n);
	gsl_matrix* H1 = gsl_matrix_alloc(n,n);

	double fx, fy, vector_dot;
	
	gsl_vector* H1y = gsl_vector_alloc(n);	
	gsl_vector* u = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* dH = gsl_matrix_alloc(n,n);
	gsl_vector* v = gsl_vector_alloc(n);

	gradient(f,x,dx,grad_f);
	gsl_matrix_set_identity(H1);
	fx=f(x);
	int nsteps=0;
	
	do{
		nsteps++;
		
		matrix_vector_prod(H1,grad_f,Dx);
		gsl_vector_scale(Dx,-1);
		gsl_vector_memcpy(s,Dx); // CHANGE
		gsl_vector_scale(s,2);
		
		double alpha=0.001;
		do{
			gsl_vector_scale(s,0.5);
			for (int i=0; i<n; i++) gsl_vector_set(z,i,gsl_vector_get(x,i)+gsl_vector_get(s,i));
			fy=f(z);
			vector_dot=0;
			for(int k=0;k<n;k++){vector_dot+=gsl_vector_get(s,k)*gsl_vector_get(grad_f,k);}
			if( fabs(fy) < fabs(fx) + alpha * vector_dot){break;}
			if(vector_norm(s) < dx) {gsl_matrix_set_identity(H1); break;}
		}while(fabs(fy) > fabs(fx) + alpha * vector_dot);
	
		gradient(f,z,dx,grad_fy);
		for (int i=0; i<n; i++) gsl_vector_set(y,i,gsl_vector_get(grad_fy,i)-gsl_vector_get(grad_f,i));
		
		matrix_vector_prod(H1,y,H1y);
		for (int i=0; i<n; i++) gsl_vector_set(u,i,gsl_vector_get(s,i)-gsl_vector_get(H1y,i));
		for (int i=0; i<n; i++) for (int j=0; j<n; j++) gsl_matrix_set(A,i,j,gsl_vector_get(u,i)*gsl_vector_get(s,j));
		matrix_prod(dH,A,H1);
		matrix_vector_prod(H1,s,v);
			vector_dot=0;
			for(int k=0;k<n;k++){vector_dot+=gsl_vector_get(y,k)*gsl_vector_get(v,k);}
		gsl_matrix_scale(dH,1/vector_dot);	
	
		gsl_matrix_add(H1,dH);
		gsl_vector_memcpy(x,z);  // CHANGE
		gsl_vector_memcpy(grad_f,grad_fy); // CHANGE
		fx=fy;
	}while(vector_norm(Dx)>dx && vector_norm(grad_f)>eps);

	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(grad_f);
	gsl_vector_free(grad_fy);
	gsl_matrix_free(H1);
	
	gsl_vector_free(H1y);
	gsl_vector_free(u);
	gsl_vector_free(v);	
	gsl_matrix_free(A);
	gsl_matrix_free(dH);
	
	return nsteps;
}

int newton(void f(gsl_vector* x, gsl_vector* fx), gsl_vector* xstart, double dx, double epsilon){
	int n=xstart->size;
	gsl_matrix* J=gsl_matrix_alloc(n,n);
	gsl_vector* Dx=gsl_vector_alloc(n);
	gsl_vector* y=gsl_vector_alloc(n);
	gsl_vector* fy=gsl_vector_alloc(n);
	gsl_vector* fx=gsl_vector_alloc(n);
	gsl_vector* df=gsl_vector_alloc(n);
	int steps=0;
	do{
		steps++;
		f(xstart,fx);
		for(int j=0; j<n;j++){
			gsl_vector_set(xstart,j,gsl_vector_get(xstart,j)+dx);
			f(xstart,df);
			gsl_vector_sub(df,fx);
			for(int i=0;i<n;i++){
				gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			}
			gsl_vector_set(xstart,j,gsl_vector_get(xstart,j)-dx);
		}
		givens_qr(J);
		gsl_vector_scale(fx,-1.0);
		givens_qr_solve(J,fx,Dx);
		gsl_vector_scale(fx,-1.0);
		double lambda=2;

		do{
			lambda/=2;
			gsl_vector_memcpy(y,Dx);
			gsl_vector_scale(y,lambda);
			gsl_vector_add(y,xstart);
			f(y,fy);
		}while(vector_norm(fy)>(1-lambda/2)*vector_norm(fx) && lambda>0.02);
			gsl_vector_memcpy(xstart,y);		
			gsl_vector_memcpy(fx,fy);
	}while(vector_norm(Dx)>dx && vector_norm(fx)>epsilon);

	gsl_matrix_free(J);
	gsl_vector_free(Dx);
	gsl_vector_free(y);
	gsl_vector_free(fy);
	gsl_vector_free(fx);
	gsl_vector_free(df);
	return steps;
}