int simplex_initiate(
	real (*fun)(real*),
	real** simplex, real* fun_values, int d,
	int* hi, int* lo, real* centroid);

void simplex_update(
		real** simplex, real* fun_values, int d,
		int* hi, int* lo, real* centroid);

void reflection(
	real* highest, real* centroid, int d,
	real* reflected);

void expansion(
	real* highest, real* centroid, int d,
	real* expanded);

void contraction(
	real* highest, real* centroid, int d,
	real* contracted);

void reduction(
	real** simplex, int d,
	int lo);

real size(real** simplex,int d);

int downhill_simplex(
        real(*fun)(real*),
        real** simplex, int d,
        real simplex_size_goal);

