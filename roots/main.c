#include<stdlib.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include<math.h>
#include<assert.h>

int newton(void f(gsl_vector* x, gsl_vector* fx), gsl_vector* xstart, double dx, double epsilon);
int newton_jac(void f(gsl_vector* x, gsl_vector* fx), void jacobian(gsl_vector* x, gsl_matrix* J), gsl_vector* xstart, double dx, double epsilon);
int newton_jac_with_g(void f(gsl_vector* x, gsl_vector* fx), void jacobian(gsl_vector* x, gsl_matrix* J), gsl_vector* xstart, double dx, double epsilon);


void funA(gsl_vector* x, gsl_vector* fx){
	int A = 10000;
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = A*x0*x1-1;
	double f1 = exp(-1*x0)+exp(-1*x1)-1-1./A;
	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);	
}

void rosenbrock(gsl_vector* x, gsl_vector* fx){
	/* This function calculates the gradient of the 
	rosenbrock which is the object function her */
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 2*(x0-1) - 400*(x1-pow(x0,2))*x0;
	double f1 = 200*(x1 - pow(x0,2));
	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);	
}

void himmelblau(gsl_vector* x, gsl_vector* fx){
	/* This function calculates the gradient of the 
	himmelblau which is the object function her */	
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(pow(x0,2)+x1-11) + 2*(x0+pow(x1,2)-7);
	double f1 = 2*(pow(x0,2)+x1-11) + 4*x1*(x0+pow(x1,2)-7);
	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);	
}

void JA(gsl_vector* x, gsl_matrix* J){
	/* This function calculates the jacobian of function A*/
	double A = 10000;
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);
	double J00, J11, J01, J10;
	J00 = A*x1;
	J01 = A*x0;
	J10 = -1*exp(-1*x0);
	J11 = -1*exp(-1*x1);
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}
void Jrosenbrock(gsl_vector* x, gsl_matrix* J){
	/* This function calculates the jacobian of gradient rosenbrock function*/
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);
	double J00, J11, J01, J10;
	J00 = 2-400*(x1-3*pow(x0,2));
	J01 = -400*x0;
	J10 = -400*x0;
	J11 = 200;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}
void Jhimmelblau(gsl_vector* x, gsl_matrix* J){
	/* This function calculates the jacobian of gradient himmelblau function */
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);
	double J00, J11, J01, J10;
	J00 = 12*pow(x0,2) + 4*x1 - 42;
	J01 = 4*(x0 + x1);
	J10 = 4*(x0 + x1);
	J11 = 12*pow(x1,2) + 4*x0 - 26;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}

int main(){
	int n = 2;
	int steps;
	double epsilon = 0.00001;
	double dx = 0.001;
	gsl_vector* xstart = gsl_vector_alloc(n);
	gsl_vector* steps_rosen = gsl_vector_alloc(3);
	gsl_vector* steps_himmel = gsl_vector_alloc(3);
	gsl_vector* steps_A = gsl_vector_alloc(3);
	gsl_vector_set(xstart,0,1);
	gsl_vector_set(xstart,1,10);

	printf("Exercise A\n\n");
	steps = newton(funA, xstart, dx,epsilon);
	printf("solving the function \nA*x*y = 1 \n exp(-x) + exp(-y) = 1 + 1/A with A = 10000 and start values (1,10) \n");
	printf("The result is: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_A,0,steps);
// Rosenbrock:	
	steps = newton(rosenbrock, xstart, dx,epsilon);
	printf("Rosenbrock function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_rosen,0,steps);

// Himmelblau
	steps = newton(himmelblau, xstart, dx,epsilon);
	printf("Himmelblau function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_himmel,0,steps);


///////////////B///////////////////////////////////
	printf("\n Exercise B: Newton method with analytical Jacobian\n");
	steps = newton_jac(funA, JA, xstart, dx,epsilon);
	printf("\nThe solution to function A: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_A,1,steps);

// Rosenbrock:	
	steps = newton_jac(rosenbrock, Jrosenbrock, xstart, dx,epsilon);
	printf("Rosenbrock function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_rosen,1,steps);

// Himmelblau
	steps = newton_jac(himmelblau, Jhimmelblau, xstart, dx,epsilon);
	printf("Himmelblau function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_himmel,1,steps);

//////////////////C////////////////////////////////
	printf("\n Exercise C: Newton method with Jacobian and fancy line search\n");
	steps = newton_jac_with_g(funA, JA, xstart, dx,epsilon);
	printf("\nfunction A: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_A,2,steps);

// Rosenbrock:	
	steps = newton_jac_with_g(rosenbrock, Jrosenbrock, xstart, dx,epsilon);
	printf("Rosenbrock function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_rosen,2,steps);

// Himmelblau
	steps = newton_jac_with_g(himmelblau, Jhimmelblau, xstart, dx,epsilon);
	printf("Himmelblau function, minimum: \n");
	printf("x=%g\n",gsl_vector_get(xstart,0));
	printf("y=%g\n",gsl_vector_get(xstart,1));
	printf("steps = %i\n", steps);
	gsl_vector_set(steps_himmel,2,steps);

	printf("\nFor comparison:\n");
	printf("The number of function calls for system A are %.0f for Newton method, %.0f for Newton method with analytical Jacobian, and %.0f for the method with fancy line search \n",gsl_vector_get(steps_A,0),gsl_vector_get(steps_A,1),gsl_vector_get(steps_A,2));
	printf("The number of function calls for the rosenbrock function are %.0f for Newton method, %.0f for Newton method with analytical Jacobian, and %.0f for the method with fancy line search \n",gsl_vector_get(steps_rosen,0),gsl_vector_get(steps_rosen,1),gsl_vector_get(steps_rosen,2));
	printf("The number of function calls for the himmelblau function are %.0f for Newton method, %.0f for Newton method with analytical Jacobian, and %.0f for the method with fancy line search \n",gsl_vector_get(steps_himmel,0),gsl_vector_get(steps_himmel,1),gsl_vector_get(steps_himmel,2));



	return 0;
}
